<?php

namespace SIAKAD\Views\Dosen;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Dosen' )
    ->set_page_name( 'Pak Dosen' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Selamat datang pak Dosen</h1>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();