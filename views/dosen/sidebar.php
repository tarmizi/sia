<?php
$obj_headers = \SIAKAD\Controller\Headers::get_instance();
$base_link = SIAKAD_URI_PATH . DS . \SIAKAD\Controller\Contents::get_instance()->get_view(); ?>
<ul class="nav nav-sidebar">
    <li <?php echo ( $page = 'Pak Dosen' ) == $obj_headers->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>"><?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Daftar Mahasiswa' ) == $obj_headers->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link . DS; ?>daftar-mahasiswa"><?php echo $page; ?></a></li>
    <li><a href="#">Nilai Mahasiswa</a></li>
    <li><a href="#">Mahasiswa Bimbingan</a></li>
</ul>