<?php

namespace SIAKAD\Views\Publik;

use SIAKAD\Controller\Footers;

Footers::get_instance()
    ->add_script( 'jquery.min.js' )
    ->add_script( 'jquery.ui.min.js' )
    ->add_script( 'bootstrap.min.js' );

?>
    <?php echo Footers::get_instance()->get_script(); ?>
</body>
</html>