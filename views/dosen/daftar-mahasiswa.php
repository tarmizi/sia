<?php

namespace SIAKAD\Views\Dosen;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Mahasiswa;

Headers::get_instance()
    ->set_page_title( 'Daftar Mahasiswa' )
    ->set_page_name( 'Daftar Mahasiswa' );

Contents::get_instance()->get_header();

$daftar_mahasiswa = Mahasiswa::get_instance()->_gets();

?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <?php Contents::get_instance()->get_sidebar(); ?>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1 class="page-header">Daftar Mahasiswa</h1>
                <table class="table">
                    <thead>
                    <tr>
                        <td>NIM</td>
                        <td>Nama</td>
                        <td>Tempat / Tanggal Lahir</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var $mahasiswa \SIAKAD\Model\Mahasiswa */
                    foreach( $daftar_mahasiswa as $mahasiswa ) : ?>
                        <tr>
                            <td><?php echo $mahasiswa->getNIM(); ?></td>
                            <td><?php echo $mahasiswa->getNama(); ?></td>
                            <td><?php echo $mahasiswa->getTempatLahir(); ?> / <?php echo $mahasiswa->getTglLahir(); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php Contents::get_instance()->get_footer();