<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */


/**
 * mengakhiri eksekusi script/halaman dengan tambahan
 * opsi tampilan
 *
 * @param string $messages
 * @param string $type
 */
function siakad_exit( $messages = '', $type = 'info' ) {

    $out = '';

    if( !empty( $messages ) ) {

        ob_start(); ?>

        <html>
        <head>
            <title>SIAKAD Universitas Mataram</title>
            <style>
                .container {
                    margin: 20px auto;
                    text-align: center;
                }
                .message {
                    color: #fff;
                    display: inline;
                    padding: 10px;
                    font-size: 11px;
                    font-family: sans-serif;
                    -moz-border-radius: 3px;
                    border-radius: 3px;
                    background-color: #008cba;
                }
                .message.info {
                    background-color: #43ac6a;
                }
                .message.alert {
                    background-color: #f04124;
                }
            </style>
        </head>
        <body>
        <div class="container">
            <p class="message <?php echo $type; ?>"><?php echo $messages; ?></p>
        </div>
        </body>
        </html>

        <?php $out = ob_get_clean();

    }

    exit( $out );
}

/**
 * berfungsi untuk me-redirect ke halaman tertentu,
 * dengan mengeksekusi exit() di akhir untuk memastikan
 * tidak ada program yang berjalan selanjutnya
 *
 * @param string $target
 */
function siakad_redirect( $target = SIAKAD_URI_PATH ) {
    header( 'Location: ' . $target );
    siakad_exit();
}

/**
 * @param $default
 * @param $destination
 * @return array
 */
function sync_default_params( $default, $destination ) {
    $return = array();
    foreach( $default as $k => $d ) {
        if( isset( $destination[ $k ] ) ) $return[ $k ] = $destination[ $k ];
        else $return[ $k ] = $d;
    }
    return $return;
}
