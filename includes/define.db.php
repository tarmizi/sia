<?php

/**
 * Sesuaikan dengan konfigurasi database lokal/server
 */

/** host database */
define( 'SIAKAD_DB_HOST', 'localhost' );
/** nama database */
define( 'SIAKAD_DB_NAME', 'siakad_unram' );
/** username */
define( 'SIAKAD_DB_USERNAME', 'YOUR_USERNAME' );
/** password */
define( 'SIAKAD_DB_PASSWORD', 'YOUR_PASSWORD' );