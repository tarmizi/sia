<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;


class Databases {

    protected $db_conn = null;
    protected $db_select = null;

    /**
     * koneksi langsung saat class di instance
     *
     * @author Zaf
     */
    function __construct() {
        $this->_connect();
    }

    /**
     * koneksi ke database
     */
    function _connect(){
        if( !$this->db_conn = mysql_connect( SIAKAD_DB_HOST, SIAKAD_DB_USERNAME, SIAKAD_DB_PASSWORD ) )
            siakad_exit( 'Ndeq ne bau konek kadu User : ' . SIAKAD_DB_USERNAME );

        if( !$this->db_select = mysql_select_db( SIAKAD_DB_NAME, $this->db_conn ) )
            siakad_exit( 'Ndek ne bau konek jok DB : ' . SIAKAD_DB_NAME );
    }

    /**
     * fungsi buat insert value
     *
     * @author Zaf
     * @param string $table
     * @param array $column
     * @param array $value
     * @return bool $res
     */
    protected function insert( $table, $column = array(), $value = array() ) {

        /** cek minimal ada 1 value yang sama dengan jumlah kolom */
        if( count( $column ) != count( $value[0] ) ) siakad_exit( 'Error while execute `insert` function, column and value count mismatch.' );

        /** query dasar */
        $query = 'INSERT INTO `' . $table . '` ( `' . implode( '`, `', $column ) . '` ) VALUES ';

        /** hitung jumlah array dari parameter value */
        $number_of_value = count( $value );

        foreach( $value as $k => $v )
            $query .= '("' . implode( '", "', $v ) . '")' . ( $k < ( $number_of_value - 1 ) ? ', ' : '' );

        return mysql_query( $query ) ? mysql_insert_id() : false;

    }

    /**
     * fungsi standar buat update value,
     *
     * @author Zaf
     * @param string $table
     * @param array $column
     * @param array $value
     * @param array $condition
     * @return bool $res
     */
    protected function update( $table, $column = array(), $value = array(), $condition = array() ) {

        if( count( $column ) != count( $value ) ) siakad_exit( 'Error while execute `update` function, column and value count mismatch.' );

        $query = 'UPDATE `' . $table . '` SET';

        foreach( $column as $k => $c )
            $query .= ' `' . $c . '` = "' . $value[ $k ] . '"' . ( $c != end( $column ) ? ', ' : '' );

        if( !empty( $condition ) )
            $query .= ' WHERE `' . $condition[ 0 ] . '` = "' . $condition[ 1 ] . '"';

        return mysql_query( $query );

    }

    /**
     * fungsi buat delete value
     *
     * @author Zaf
     * @param string $table
     * @param string $column
     * @param string $value
     * @return bool $res
     */
    protected function delete( $table, $column, $value ) {

        $query = 'DELETE FROM `' . $table . '` WHERE `' . $column . '` = "' . $value . '"';

        return mysql_query( $query );

    }
} 