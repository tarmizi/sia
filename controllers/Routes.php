<?php

/**
 * Class ini berisi fungsi untuk membuat parameter
 * menjadi lebih user friendly URL
 *
 * mungkin akan berisi banyak conditional statement,
 * tapi bisa disesuaikan dengan kebutuhan
 */

namespace SIAKAD\Controller;

class Routes {

    private $tingkat1;
    private $tingkat2;
    private $tingkat3;
    private $tingkat4;
    private $tingkat5;

    /**
     * tentukan views apa saja yang akan digunakan,
     * dalam hal ini kita gunakan $tingkat1 sebagai type untuk view
     */
    const view_admin = 'admin';
    const view_akademik = 'akademik';
    const view_dosen = 'dosen';
    const view_operator = 'operator';
    const view_mahasiswa = 'mahasiswa';
    const view_beranda = 'beranda';

    const script_ext = '.php';
    const page_index = 'index.php';
    const page_404 = '404.php';

    /** @var Routes $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param $params
     */
    function parse_page( $params ) {

        /**
         * karena sementara kita hanya menggunakan parameter
         * sebanyak 5 tingkat
         */
        $this->tingkat1 = isset( $params[ 'tingkat1' ] ) ? $params[ 'tingkat1' ] : '';
        $this->tingkat2 = isset( $params[ 'tingkat2' ] ) ? $params[ 'tingkat2' ] : '';
        $this->tingkat3 = isset( $params[ 'tingkat3' ] ) ? $params[ 'tingkat3' ] : '';
        $this->tingkat4 = isset( $params[ 'tingkat4' ] ) ? $params[ 'tingkat4' ] : '';
        $this->tingkat5 = isset( $params[ 'tingkat5' ] ) ? $params[ 'tingkat5' ] : '';

        $view_path = SIAKAD_VIEW_ABS_PATHH;
        $file_to_include = $view_path . DS . self::page_404;

        if( empty( $this->tingkat1 ) || $this->tingkat1 == self::view_beranda ) {
            $file_to_include = $view_path . DS . self::page_index;
        } else {
            $has_view = true;
            switch( $this->tingkat1 ) {
                case self::view_admin :
                    Contents::get_instance()->set_view( self::view_admin ); break;
                case self::view_akademik :
                    Contents::get_instance()->set_view( self::view_akademik ); break;
                case self::view_dosen :
                    Contents::get_instance()->set_view( self::view_dosen ); break;
                case self::view_operator :
                    Contents::get_instance()->set_view( self::view_operator ); break;
                case self::view_mahasiswa :
                    Contents::get_instance()->set_view( self::view_mahasiswa ); break;
                default :
                    $has_view = false;
            }
            if( $has_view )
                $file_to_include = Contents::get_instance()->get_view_path() . DS .
                    ( empty( $this->tingkat2 ) ? self::page_index : $this->tingkat2 . self::script_ext );
        }

        /** cek sekali lagi */
        if( !is_readable( $file_to_include ) ) {
            $file_to_include = $view_path . DS . self::page_404;

            /**
             * jika memang file untuk di-include tidak ditemukan
             * bersihkan variabel view yang sudah kedung tersimpan
             * di objek Contents
             */
            Contents::get_instance()->remove_view();
        }

        //siakad_exit( $file_to_include );

        /** mulai meng-include */
        if( is_readable( $file_to_include ) ) include( $file_to_include );

        /** bahkan jika file 404.php tidak ada */
        else siakad_exit( 'Script yang side eksekusi belum ada.', 'alert' );

    }

} 